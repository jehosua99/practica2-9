#include <iostream>
/*
 al programa se le ingresara un numero y este lo dividira en numeros de tres cifras que seran sumadas; el resultado de la suma
 sera entregado al finalizar el programa.
*/
using namespace std;

int numerodividido(int);  //Funcion en donde se dividira el numero en 3 cifras
int main()
{
    int numeroingresado, cadena[20], posicion = 0, total = 0;
    cout << "Ingrese el numero que sera sumado:\n";
    cin >> numeroingresado;
    while (numeroingresado != 0)  //Cuando el numero se haga 0 sera porque no tenga mas cifras
        {
            cadena[posicion] = {numerodividido(numeroingresado)};  //Guardo en cada posicion de la cadena empezando desde 0 el numero de 3 cifras resultante
            numeroingresado /= 1000;   //Divido el numero original entre 100 para eliminar sus ultimas tres cifras
            total += *(cadena+posicion);   //Sumo los numeros de 3 cifras
            posicion ++;  //Aumento la variable posicion para guardar el numero que viene en la posicion de memoria siguiente
        }
        cout << "El resultado de la suma es: " << total << endl;
    return 0;
}

int numerodividido (int a)
{
    int valor1, valor2, valor3, resultado;
    valor1 = a % 10;  //Guardo el valor de la ultima cifra del numero ingresado en una variable (asi con las variables "valor2" y "valor3"
    a /= 10;   //Divido al numero entre 10 para eliminar su ultima cifra
    valor2 = a % 10;
    a /= 10;
    valor3 = a % 10;
    a /= 10;
    resultado = (valor3*100) + (valor2*10) + valor1;   //El valor3 se multiplica por 100 porque es el que ocupara el lugar de las centenas dentro de la suma, asi mismo con valor2 pero en las decenas y finalmente el ultimo valor al ser una unidad no debera multiplicarse
    return resultado;
}
